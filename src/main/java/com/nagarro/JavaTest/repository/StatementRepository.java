package com.nagarro.JavaTest.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.nagarro.JavaTest.models.Statement;

@Repository
public interface StatementRepository extends CrudRepository<Statement, Long> {
 List<Statement> findAll();
}
