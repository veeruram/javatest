package com.nagarro.JavaTest.repository;

import org.springframework.data.repository.CrudRepository;

import com.nagarro.JavaTest.models.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {

}
