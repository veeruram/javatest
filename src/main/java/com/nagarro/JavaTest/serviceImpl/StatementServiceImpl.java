package com.nagarro.JavaTest.serviceImpl;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nagarro.JavaTest.exception.DatabaseException;
import com.nagarro.JavaTest.models.Statement;
import com.nagarro.JavaTest.repository.StatementRepository;
import com.nagarro.JavaTest.service.StatementService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Service
@RequiredArgsConstructor
@Slf4j
public class StatementServiceImpl implements StatementService{
	
	@NonNull
	StatementRepository statementRepository;
	
	@Override
	public List<Statement> getDefaultStatement() throws DatabaseException {
		
		List<Statement> statements = new ArrayList<Statement>();
		try {
			statements = statementRepository.findAll();
		} catch (Exception e) {
			
			log.error("StatementServiceImpl.getDefaultStatement Error in Database -- >", e);
			throw new DatabaseException(e.getMessage());
		}
		List<Statement> defaultList = statements.stream().filter(x->{
			return x.getDateValue().isAfter(LocalDate.now().minusMonths(24));
		}).collect(Collectors.toList());
		return defaultList;
	}

	@Override
	public List<Statement> getStatement(String accountId, LocalDate fromDate, LocalDate toDate, Double amountFrom,
			Double amountTo) throws DatabaseException {
		List<Statement> statements = new ArrayList<Statement>();
		try {
			statements = statementRepository.findAll();
		} catch (Exception e) {
			
			log.error("StatementServiceImpl.getStatement Error in Database -- >", e);
			throw new DatabaseException(e.getMessage());
		}
		if(null!=accountId) {
			 Predicate<Statement> account = s ->! s.getAccount().getAccountNumber().equals(accountId);
			statements.removeIf(account);
		}
	     if(null!=fromDate) {
	    	 Predicate<Statement> dateRangefrm = s -> !s.getDateValue().isAfter(fromDate);
	    	 statements.removeIf(dateRangefrm); 	 
	     }
	     if(null!=toDate) {
	    	 Predicate<Statement> dateRangeTo = s -> !s.getDateValue().isBefore(toDate);
	    	 statements.removeIf(dateRangeTo); 	 
	     }
	     if(null!=amountFrom) {
	    	
	    	 Predicate<Statement> amountRangeFrom = s -> !(s.getAmount()>=amountFrom);
	    	 statements.removeIf(amountRangeFrom); 
	     }
	     if(null!=amountTo) {
		    	
	    	 Predicate<Statement> amountRangeTo = s -> !(s.getAmount()<=amountTo);
	    	 statements.removeIf(amountRangeTo); 
	     }
	     
		return statements;
	}

	

}
