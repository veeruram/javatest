package com.nagarro.JavaTest.exception;

public class DatabaseException extends Exception{
	
	 public DatabaseException(String errorMessage, Throwable err) {
	        super(errorMessage, err);
	    }
	 public DatabaseException(String errorMessage) {
	        super(errorMessage);
	    }

}