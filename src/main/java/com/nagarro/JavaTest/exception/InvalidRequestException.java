package com.nagarro.JavaTest.exception;

public class InvalidRequestException extends Exception{
	
	 public InvalidRequestException(String errorMessage, Throwable err) {
	        super(errorMessage, err);
	    }
	 public InvalidRequestException(String errorMessage) {
	        super(errorMessage);
	    }

}