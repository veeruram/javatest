package com.nagarro.JavaTest.exception;

public class UnAuthorizedException extends Exception{
	
	 public UnAuthorizedException(String errorMessage, Throwable err) {
	        super(errorMessage, err);
	    }
	 public UnAuthorizedException(String errorMessage) {
	        super(errorMessage);
	    }

}