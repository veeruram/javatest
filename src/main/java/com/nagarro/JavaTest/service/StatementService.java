package com.nagarro.JavaTest.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.nagarro.JavaTest.exception.DatabaseException;
import com.nagarro.JavaTest.models.Statement;

@Service
public interface StatementService {
	
	List<Statement> getDefaultStatement() throws DatabaseException;

	List<Statement> getStatement(String accountId, LocalDate fromDate, LocalDate toDate, Double amountFrom, Double amountTo) throws DatabaseException;

}
