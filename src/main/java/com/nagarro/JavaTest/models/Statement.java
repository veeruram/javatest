package com.nagarro.JavaTest.models;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.extern.java.Log;

@Log
@Data
@Entity
public class Statement {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;
	private String datefield;
	private double amount;
	@Transient @JsonIgnore
	private LocalDate dateValue;
	
	public LocalDate getDateValue() {
		
		return LocalDate.parse(datefield, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
	}
	
	

}
