package com.nagarro.JavaTest.models;

public class RestErrorEntity {

    private final int httpCode;

    private final String message;

    private final String errorCode;


    public RestErrorEntity(final int httpCode, final String message, final String errorCode) {
        this.httpCode = httpCode;
        this.message = message;
        this.errorCode = errorCode;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }
}

