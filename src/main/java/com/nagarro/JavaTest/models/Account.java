package com.nagarro.JavaTest.models;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Data
@Entity
public class Account {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	int id;
	@JsonIgnore
	String accountNumber;
	String accountType;
	@Transient
	int accountNumberHash;
	
	//return hash value
	public int getAccountNumberHash() {
		return this.accountNumber.hashCode();
		
	}

}
