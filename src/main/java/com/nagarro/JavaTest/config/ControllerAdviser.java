package com.nagarro.JavaTest.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nagarro.JavaTest.exception.DatabaseException;
import com.nagarro.JavaTest.exception.InvalidRequestException;
import com.nagarro.JavaTest.exception.UnAuthorizedException;
import com.nagarro.JavaTest.models.RestErrorEntity;

@ControllerAdvice
public class ControllerAdviser extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(InvalidRequestException.class)
	public ResponseEntity<RestErrorEntity> handleException(InvalidRequestException e){
		
		return new ResponseEntity<RestErrorEntity>(new RestErrorEntity(HttpStatus.BAD_REQUEST.value(), e.getMessage(), HttpStatus.BAD_REQUEST.getReasonPhrase()), HttpStatus.BAD_REQUEST);
		
	}
	
	@ExceptionHandler(UnAuthorizedException.class)
	public ResponseEntity<RestErrorEntity> handleException(UnAuthorizedException e){
		
		return new ResponseEntity<RestErrorEntity>(new RestErrorEntity(HttpStatus.UNAUTHORIZED.value(), e.getMessage(), HttpStatus.UNAUTHORIZED.getReasonPhrase()), HttpStatus.UNAUTHORIZED);
		
	}
	
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<RestErrorEntity> handleException(MethodArgumentTypeMismatchException e){
		
		return new ResponseEntity<RestErrorEntity>(new RestErrorEntity(HttpStatus.BAD_REQUEST.value(), e.getMessage(), HttpStatus.BAD_REQUEST.getReasonPhrase()), HttpStatus.BAD_REQUEST);
		
	}
	
	@ExceptionHandler(DatabaseException.class)
	public ResponseEntity<RestErrorEntity> handleException(DatabaseException e){
		
		return new ResponseEntity<RestErrorEntity>(new RestErrorEntity(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()), HttpStatus.INTERNAL_SERVER_ERROR);
		
	}

}