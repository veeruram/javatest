package com.nagarro.JavaTest.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;


import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

public class SwaggerConfig {
	
	@Bean
	public Docket api() {                
	    return new Docket(DocumentationType.SWAGGER_2)          
	      .select()
	      .apis(RequestHandlerSelectors.basePackage("com.nagarro.JavaTest"))
	      .build()
	      .apiInfo(new ApiInfo("Statement API", "Account Statement API for Nagarro", "0.1", null, new Contact("John Doe", "www.example.com", "myeaddress@company.com"), 
	    	      "License of API", "API license URL", Collections.emptyList()));
	}
	
	


}
