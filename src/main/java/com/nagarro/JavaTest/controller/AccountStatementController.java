package com.nagarro.JavaTest.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.JavaTest.exception.DatabaseException;
import com.nagarro.JavaTest.exception.UnAuthorizedException;
import com.nagarro.JavaTest.models.Statement;
import com.nagarro.JavaTest.service.StatementService;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
@Secured({"ROLE_ADMIN", "ROLE_USER"})
class AccountStatementController {

	@NonNull
   StatementService statementService;

 
 
  @GetMapping("v1.0/statement")
  List<Statement> getStatements(@RequestParam(required = false) String accountId, @RequestParam(required = false) @DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate fromDate, @RequestParam(required = false)@DateTimeFormat(pattern = "dd.MM.yyyy") LocalDate toDate, @RequestParam(required = false) Double amountFrom, @RequestParam(required = false) Double amountTo) throws UnAuthorizedException, DatabaseException {
	  log.info("{} --> Start AccountStatementController.getStatements", LocalDateTime.now());
	  Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	  if (auth != null && auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_USER"))) {
		  if( null==accountId && null==fromDate && null==toDate && null==amountFrom && null==amountTo) {
				
			  return statementService.getDefaultStatement();
			}else {
				log.error("{} -->User does not have authority to querry with parameters",auth.getName());
				throw new UnAuthorizedException("User does not have authority to querry with parameters");
			}
		 
	  }else if(null!=accountId ||null!=fromDate || null!=toDate || null!=amountFrom||null!=amountTo) {
		  return statementService.getStatement(accountId,fromDate, toDate, amountFrom, amountTo);
	  }
	  log.info("{} --> End AccountStatementController.getStatements", LocalDateTime.now());
    return statementService.getDefaultStatement();
  }



}
