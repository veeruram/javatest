package com.nagarro.JavaTest;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.nagarro.JavaTest.models.Statement;
import com.nagarro.JavaTest.repository.StatementRepository;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class JavaTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaTestApplication.class, args);
	}
	  @Bean
	  public CommandLineRunner demo(StatementRepository repository) {
	    return (args) -> {
	      

	    	List<Statement> res =repository.findAll();
	    	
	    	List<Statement> range = res.parallelStream().filter(x->{
				return x.getDateValue().isAfter(LocalDate.ofYearDay(2001, 1));
				
			}).collect(Collectors.toList());
	      for (Statement statement : range) {
	        System.out.println(statement.getAccount().getAccountNumberHash());
	      }
	      
	    };
	  }
}
