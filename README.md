# Statement-API-App#

This repository contains the code for the Java Assignment

### Pre-requisite ###

Maven 3
Java 11

### Contribution ###

The following functionalities have been implementes

Access DB Repository classes

*login/logout - using spring security
*http://localhost:8080/login
*http://localhost:8080/logout
*Validation for concurrent session

###Statement API##

*Role based Authorization
*Parameter validations
*http://localhost:8080/swagger



###To do###
*Junit,
*Sonarqube
*Jacoco- Java code coverage